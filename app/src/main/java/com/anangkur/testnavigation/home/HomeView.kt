package com.anangkur.testnavigation.home

import anangkur.com.beritakan.models.Article

interface HomeView{
    fun showDataFromAPI(list: List<Article>)
}