package com.anangkur.testnavigation.home

import anangkur.com.beritakan.models.Article
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.anangkur.testnavigation.MainActivity
import com.anangkur.testnavigation.R
import com.anangkur.testnavigation.home.adapter.BeritaAdapter

class HomeFragment: Fragment(), HomeView{

    lateinit var homeLayout: SwipeRefreshLayout
    lateinit var recyclerview: RecyclerView
    private var listArticle: MutableList<Article> = mutableListOf()
    private val presenter = HomePresenter(this)
    private lateinit var adapter: BeritaAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        defineViews(view)
        initPresenter()
        setAdapter()
    }

    private fun defineViews(view: View){
        homeLayout = view.findViewById(R.id.home_layout)
        recyclerview = view.findViewById(R.id.recyclerview)
    }

    private fun initPresenter(){
        presenter.getBreakingNews()
        homeLayout.setOnRefreshListener {
            presenter.getBreakingNews()
        }
    }

    private fun setAdapter(){
        adapter = context?.let {
            BeritaAdapter(
                listArticle,
                it
            )
        }!!
        val layoutManagerBestSeller = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerview.layoutManager = layoutManagerBestSeller
        recyclerview.itemAnimator = DefaultItemAnimator()
        recyclerview.adapter = adapter
    }

    override fun showDataFromAPI(list: List<Article>) {
        homeLayout.isRefreshing = false
        listArticle.clear()
        listArticle.addAll(list)
        adapter.notifyDataSetChanged()
    }
}