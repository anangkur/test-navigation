package com.anangkur.testnavigation.home

import anangkur.com.beritakan.models.ResponseApiHome
import anangkur.com.beritakan.views.api.NewsApiService
import anangkur.com.beritakan.views.api.ServiceGenerator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter(private val homeView: HomeView){

    fun getBreakingNews() {
        val newsApiService = ServiceGenerator.createService(NewsApiService::class.java)
        newsApiService?.getHeadlines("id", "261d82dd7e26494e841fb1039a4fdaf7")?.enqueue(object :
            Callback<ResponseApiHome> {
            override fun onResponse(call: Call<ResponseApiHome>, response: Response<ResponseApiHome>) {
                if (response.isSuccessful){
                    val breakingData = response.body()
                    if (breakingData?.status.equals("ok")){
                        breakingData?.articles?.let { homeView.showDataFromAPI(it) }
                    }
                }
            }

            override fun onFailure(call: Call<ResponseApiHome>, t: Throwable) {
                t.printStackTrace()
                println("error get headlines!")
            }
        })
    }
}