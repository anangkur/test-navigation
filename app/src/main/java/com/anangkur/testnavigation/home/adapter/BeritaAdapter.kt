package com.anangkur.testnavigation.home.adapter

import anangkur.com.beritakan.models.Article
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import com.anangkur.testnavigation.R
import com.anangkur.testnavigation.home.HomeFragmentDirections
import com.squareup.picasso.Picasso

class BeritaAdapter(private val list: List<Article>, private val context: Context): RecyclerView.Adapter<BeritaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.item_berita, p0, false)
        val holderData = ViewHolder(v)

        return holderData
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.setDataToView(list[p1])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var img_item_berita: ImageView
        var txt_title_item_berita: TextView
        lateinit var berita: Article

        init {
            itemView.setOnClickListener(this)
            img_item_berita = itemView.findViewById(R.id.img_item_berita) as ImageView
            txt_title_item_berita = itemView.findViewById(R.id.txt_title_item_berita) as TextView
        }

        fun setDataToView(berita: Article){
            Picasso.with(context)
                .load(berita.urlToImage)
                .fit()
                .centerCrop()
                .into(img_item_berita)
            txt_title_item_berita.text = berita.title
            this.berita = berita
        }

        override fun onClick(view: View) {
            val action = HomeFragmentDirections.ActionHomeFragmentToIsiBeritaFragment()
            action.setTitle(berita.title.toString())
            action.setImage(berita.urlToImage.toString())
            action.setContent(berita.content.toString())
            view.findNavController().navigate(action)
        }
    }
}