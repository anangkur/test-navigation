package com.anangkur.testnavigation.isi_berita

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.anangkur.testnavigation.R
import com.squareup.picasso.Picasso

class IsiBeritaFragment: Fragment(), IsiBeritaView{

    lateinit var txt_title_berita: TextView
    lateinit var img_berita: ImageView
    lateinit var txt_isi_berita: TextView

    private val isiBeritaPresenter = IsiBeritaPresenter(this)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail_berita, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        defineViews(view)
        initPresenter()
    }

    private fun defineViews(view: View){
        txt_title_berita = view.findViewById(R.id.txt_title_berita)
        img_berita = view.findViewById(R.id.img_berita)
        txt_isi_berita = view.findViewById(R.id.txt_isi_berita)
    }

    private fun initPresenter(){
        isiBeritaPresenter.getBerita(IsiBeritaFragmentArgs.fromBundle(arguments).title,
            IsiBeritaFragmentArgs.fromBundle(arguments).image,
            IsiBeritaFragmentArgs.fromBundle(arguments).content)
    }

    override fun showDataBerita(title: String, image: String, content: String) {
        txt_title_berita.text = title
        Picasso.with(context)
            .load(image)
            .fit()
            .centerCrop()
            .error(R.color.colorAccent)
            .into(img_berita)
        txt_isi_berita.text = content
    }
}