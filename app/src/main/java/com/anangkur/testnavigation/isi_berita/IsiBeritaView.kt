package com.anangkur.testnavigation.isi_berita

interface IsiBeritaView{
    fun showDataBerita(title: String, image: String, content: String)
}