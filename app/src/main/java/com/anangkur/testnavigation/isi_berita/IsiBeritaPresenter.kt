package com.anangkur.testnavigation.isi_berita

class IsiBeritaPresenter(private val isiBeritaView: IsiBeritaView){
    fun getBerita(title: String, image: String, content: String){
        isiBeritaView.showDataBerita(title, image, content)
    }
}