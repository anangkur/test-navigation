package com.anangkur.testnavigation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.LinearLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity(){

    lateinit var toolbar: Toolbar
    lateinit var navigationFragment: NavHostFragment
    lateinit var main_layout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        defineViews()
        setUpToolbar(toolbar)
        setupActionBar()
    }

    private fun defineViews(){
        toolbar = findViewById(R.id.toolbar)
        navigationFragment = supportFragmentManager.findFragmentById(R.id.navigation_fragment) as NavHostFragment? ?:return
    }

    private fun setUpToolbar(toolbar: Toolbar){
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun setupNavigationMenu(navController: NavController) {
        val sideNavView = findViewById<BottomNavigationView>(R.id.nav_view)
        NavigationUI.setupWithNavController(sideNavView, navController)
    }

    private fun setupActionBar() {
        val host: NavHostFragment = supportFragmentManager.findFragmentById(R.id.navigation_fragment) as NavHostFragment? ?: return
        val navController = host.navController
        setupActionBarWithNavController(navController, null)
        setupNavigationMenu(navController)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(findNavController(R.id.navigation_fragment)) || super.onOptionsItemSelected(item)
    }


}
