package com.anangkur.testnavigation.utils

import android.app.Application
import com.google.gson.Gson

class TestNavigationApp : Application() {

    override fun onCreate() {
        super.onCreate()
        //Nambahin Fabric
    }

    companion object {
        var gson: Gson

        init {
            gson = Gson()
        }
    }
}