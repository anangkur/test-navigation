package com.anangkur.testnavigation.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.anangkur.testnavigation.models.ResponseFbLogin

object SharedPreferencesHelper {

    private fun getSP(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun getPref(context: Context, key: String): String? {
        return getSP(context).getString(key, null)
    }

    /*SAVE AND GET DATA LOGIN*/
    fun saveDataLogin(context: Context, usersModel: ResponseFbLogin?) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
            .putString("account", TestNavigationApp.gson.toJson(usersModel)).apply()
    }

    fun getDataUser(context: Context): ResponseFbLogin? {
        return TestNavigationApp.gson.fromJson(getPref(context, "account"), ResponseFbLogin::class.java)
    }

    fun deleteDataUser(context: Context){
        PreferenceManager.getDefaultSharedPreferences(context).edit().clear().commit()
    }
}