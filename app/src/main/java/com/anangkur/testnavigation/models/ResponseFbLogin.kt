package com.anangkur.testnavigation.models

data class ResponseFbLogin(
    val birthday: String,
    val email: String,
    val gender: String,
    val id: String,
    val name: String
)