package com.anangkur.testnavigation.profile

import com.anangkur.testnavigation.models.ResponseFbLogin

interface ProfileView{
    fun setDataProfile(responseFbLogin: ResponseFbLogin)
    fun onSuccessFbLogin(responseFbLogin: ResponseFbLogin)
    fun onCancelFbLogin()
    fun onFailedFbLogin()
}