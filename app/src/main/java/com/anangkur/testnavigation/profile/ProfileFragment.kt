package com.anangkur.testnavigation.profile

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.fragment.findNavController
import com.anangkur.testnavigation.R
import com.anangkur.testnavigation.models.ResponseFbLogin
import com.anangkur.testnavigation.utils.SharedPreferencesHelper
import com.facebook.CallbackManager
import com.facebook.login.LoginManager
import com.facebook.login.widget.LoginButton
import com.squareup.picasso.Picasso

class ProfileFragment: Fragment(), ProfileView{

    lateinit var img_profile: ImageView
    lateinit var txt_nama: TextView
    lateinit var txt_birthday: TextView
    lateinit var txt_gender: TextView
    lateinit var btn_logout: RelativeLayout
    lateinit var txt_email: TextView
    lateinit var active_layout: LinearLayout
    lateinit var empty_layout: LinearLayout
    lateinit var loginButton: LoginButton

    val callbackManager: CallbackManager = CallbackManager.Factory.create()

    private val presenter = ProfilePresenter(this)

    override fun setDataProfile(responseFbLogin: ResponseFbLogin) {
        Picasso.with(context)
            .load("https://graph.facebook.com/${responseFbLogin.id}/picture?type=large")
            .fit()
            .centerCrop()
            .into(img_profile)
        txt_nama.text = responseFbLogin.name
        txt_birthday.text = responseFbLogin.birthday
        txt_gender.text = responseFbLogin.gender
        txt_email.text = responseFbLogin.email
        active_layout.visibility = View.VISIBLE
        empty_layout.visibility = View.GONE
    }

    override fun onSuccessFbLogin(responseFbLogin: ResponseFbLogin) {
        context?.let { SharedPreferencesHelper.saveDataLogin(it, responseFbLogin) }
        val responseFbLogin = context?.let { SharedPreferencesHelper.getDataUser(it) }
        println("sukses menyimpan data ke lokal..")
        println("id: ${responseFbLogin?.id}")
        println("nama: ${responseFbLogin?.name}")
        println("email: ${responseFbLogin?.email}")
        println("birthday: ${responseFbLogin?.birthday}")
        println("gender: ${responseFbLogin?.gender}")
        Toast.makeText(context, "login berhasil", Toast.LENGTH_SHORT).show()
        context?.let { presenter.getDataUser(it) }
        /*findNavController().navigate(ProfileFragmentDirections.ActionProfileFragmentToHomeFragment())*/
    }

    override fun onCancelFbLogin() {
        Toast.makeText(context, "login batal", Toast.LENGTH_SHORT).show()
    }

    override fun onFailedFbLogin() {
        Toast.makeText(context, "login gagal", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        defineViews(view)
        presenter.getDataLoginFb(loginButton, callbackManager)
        if (isLoggedin()){
            context?.let { presenter.getDataUser(it) }
        }else{
            active_layout.visibility = View.GONE
            empty_layout.visibility = View.VISIBLE
        }
    }

    private fun defineViews(view: View){
        img_profile = view.findViewById(R.id.img_profile)
        txt_nama = view.findViewById(R.id.txt_nama)
        txt_birthday = view.findViewById(R.id.txt_birthday)
        txt_gender = view.findViewById(R.id.txt_gender)
        btn_logout = view.findViewById(R.id.btn_logout)
        txt_email = view.findViewById(R.id.txt_email)
        active_layout = view.findViewById(R.id.active_layout)
        empty_layout = view.findViewById(R.id.empty_layout)

        loginButton = view.findViewById(R.id.login_button)
        loginButton.fragment = this

        btn_logout.setOnClickListener { onClickBtnLogout() }
    }

    private fun onClickBtnLogout(){
        LoginManager.getInstance().logOut()
        context?.let { SharedPreferencesHelper.deleteDataUser(it) }
        Toast.makeText(context, "logout berhasil",Toast.LENGTH_SHORT).show()
        active_layout.visibility = View.GONE
        empty_layout.visibility = View.VISIBLE
    }

    private fun isLoggedin(): Boolean{
        if (context?.let { SharedPreferencesHelper.getDataUser(it) } != null){
            println("user login: ${SharedPreferencesHelper.getDataUser(context!!)?.name}")
            return true
        }else{
            return false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}