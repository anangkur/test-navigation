package com.anangkur.testnavigation.profile

import android.content.Context
import android.os.Bundle
import android.util.Log
import com.anangkur.testnavigation.models.ResponseFbLogin
import com.anangkur.testnavigation.utils.SharedPreferencesHelper
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import java.util.*

class ProfilePresenter(private val profileView: ProfileView){
    fun getDataUser(context: Context){
        val responseFbLogin:ResponseFbLogin? = SharedPreferencesHelper.getDataUser(context)
        responseFbLogin?.let { profileView.setDataProfile(it) }
    }

    fun getDataLoginFb(loginButton: LoginButton, callbackManager: CallbackManager) {
        loginButton.setReadPermissions(Arrays.asList("email","user_gender","user_birthday"))
        loginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                    Log.v("LoginActivity", response.toString())
                    profileView.onSuccessFbLogin(ResponseFbLogin(
                        `object`.getString("birthday"),
                        `object`.getString("email"),
                        `object`.getString("gender"),
                        `object`.getString("id"),
                        `object`.getString("name")
                    ))
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender,birthday")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                profileView.onCancelFbLogin()
            }

            override fun onError(exception: FacebookException) {
                profileView.onFailedFbLogin()
            }
        })
    }
}