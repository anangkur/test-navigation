package anangkur.com.beritakan.views.api

import anangkur.com.beritakan.models.ResponseApiHome
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiService {
    @GET("top-headlines")
    fun getHeadlines(@Query("country") country: String, @Query("apiKey") apiKey: String): Call<ResponseApiHome>

    @GET("top-headlines")
    fun getNewsByCategories(@Query("country") country: String, @Query("apiKey") apiKey: String, @Query("category") category: String): Call<ResponseApiHome>

    /*@GET("everything")
    fun getNewsBySearch(@Query("q") q: String, @Query("apiKey") apiKey: String): Call<ResponseApiHome>*/
}