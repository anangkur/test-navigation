package anangkur.com.beritakan.views.api

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {
    private var retrofit: Retrofit? = null
    private var builder: Retrofit.Builder? = null

    fun getClient(baseURL: String): Retrofit? {
        if (retrofit == null) {
            retrofit = getBuilder(baseURL)?.build()
        }
        return retrofit
    }

    fun getBuilder(baseURL: String): Retrofit.Builder? {
        if (builder == null) {
            val gson = GsonBuilder()
                .setLenient()
                .create()
            builder = Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create(gson))
        }
        return builder
    }
}
